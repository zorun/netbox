from __future__ import unicode_literals

import re

import requests
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from django.db import transaction
from django.utils.text import slugify

from dcim.models import Facility


class Command(BaseCommand):
    help = "Import facilities from PeeringDB, with optional filtering"

    def add_arguments(self, parser):
        parser.add_argument('--min-networks', type=int, help="Only keep facilities with at least this number of networks")
        parser.add_argument('--country', '-c', help="Only keep facilities for the given country code (e.g. 'FR')")

    def handle(self, *args, **options):
        url = 'https://peeringdb.com/api/fac'
        if options['country']:
            url += '?country=' + options['country']
        r = requests.get(url=url)
        count = 0
        for fac in r.json()['data']:
            if options['min_networks'] and fac['net_count'] < options['min_networks']:
                continue
            # Try to parse peeringDB name, to have a short name and an optional description
            name = fac['name']
            description = list()
            while True:
                match = re.match(r'(.*) \((.*?)\)', name)
                if not match:
                    break
                groups = match.groups()
                name = groups[0]
                description.insert(0, groups[1])
            for d in description:
                if len(d) <= 4:
                    name += ' ({})'.format(d)
            description = ', '.join([d for d in description if len(d) > 4])
            address = "{}\n{}\n{} {}\n{}".format(fac['address1'],
                                                 fac['address2'],
                                                 fac['zipcode'],
                                                 fac['city'],
                                                 fac['country'])
            f = Facility(name=name,
                         slug=slugify(name),
                         description=description,
                         peeringdb_id=fac['id'],
                         city=fac['city'],
                         latitude=fac['latitude'],
                         longitude=fac['longitude'],
                         physical_address=address)
            f.save()
            count += 1
        self.stdout.write("Finished, import {} facilities!".format(count))
